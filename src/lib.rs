#![feature(core_intrinsics)]
extern crate rand;
use std::error::Error;
use std::fs::{File, OpenOptions};  
use std::io::{prelude::*, SeekFrom};
use std::ops::{Shl};

pub const RICH_HEADER_OFFSET : u64 = 0x80;
pub const RICH_TAG : [u8;4] = [0x52, 0x69, 0x63, 0x68]; // R i c h $$$$
pub const RICH_PRELUDE : [u32;4] = [0x44616e53,0,0,0];


/// The ImageDosHeader structure defines the needed offsets in the dos header
/// to find the offset to the PE header
#[derive(Clone, Copy)]
#[repr(C)]
pub struct ImageDosHeader {
    e_magic : u16,
    unused : [u16;29],
    e_lfanew : u32
}

impl ImageDosHeader {
    pub fn to_bytes(&self) -> Vec<u8>{
        let mut ret : Vec<u8> = Vec::new();

        ret.append(
            &mut self.e_magic.to_le_bytes().to_vec()
        );

        for val in self.unused {
            ret.append(
              &mut val.to_le_bytes().to_vec()  
            );
        }

        ret.append(
            &mut self.e_lfanew.to_le_bytes().to_vec()
        );

        ret
    }
}

#[derive(Clone)]
pub struct ImageDosHeaderStub {
    content : Vec<u8>
}

impl ImageDosHeaderStub {
    pub fn to_bytes(&self) -> Vec<u8>{
        self.content.clone()
    }
}

/// The RichHeaderEntry structure represents a single header entry
/// in the Rich header following the 16B prelude and until the "Rich" terminating tag.
/// 
/// When writing these entries to a file manually ensure the build, prog_id, and count
/// fields are converted to little-endian.
#[derive(Clone)]
#[repr(C)]
pub struct RichHeaderEntry {
    pub build : u16,
    pub prog_id : u16,
    pub count : u32
}

/// The FullDosRichHeader contains the DOS header, the DOS header stub, and the Rich header.
#[derive(Clone)]
pub struct FullDosRichHeader {
    dos_hdr : ImageDosHeader,
    dos_hdr_stub : ImageDosHeaderStub,
    rich_hdr : RichHeader
}

impl FullDosRichHeader {
    pub fn to_bytes(&self) -> Vec<u8> {
        let mut ret :Vec<u8> = Vec::new();
        
        ret.append(&mut self.dos_hdr.to_bytes());
        ret.append(&mut self.dos_hdr_stub.to_bytes());
        ret.append(&mut self.rich_hdr.to_bytes());

        ret
    }

    pub fn to_bytes_encoded(&mut self, checksum : u32) -> Vec<u8> {
        let mut ret :Vec<u8> = Vec::new();
        
        ret.append(&mut self.dos_hdr.to_bytes());
        ret.append(&mut self.dos_hdr_stub.to_bytes());
        ret.append(&mut self.rich_hdr.to_bytes_encoded(checksum));

        ret
    }

    pub fn get_checksum(&self) -> u32 {
        // https://github.com/trailofbits/pe-parse/pull/94/commits/04e8d0e72a285e936e1cd8b55ee02ca80b56c04c
        let mut checksum : u32 = 0;

        let mut full_dos = self.dos_hdr.clone().to_bytes();
        full_dos.extend(self.dos_hdr_stub.clone().to_bytes());

        let rich_entries = self.rich_hdr.rich_header_entries.clone();

        for i in 0..full_dos.len() {
            if i >= 0x3c && i <= 0x3f {
                continue;
            }

            checksum = checksum.wrapping_add((full_dos[i] as u32).rotate_left((i & 0x1f) as u32));
        }

        for i in 0..rich_entries.len() {
            let entry = &rich_entries[i];
            let x : u32 = (entry.prog_id as u32).shl(16) | entry.build as u32;
            let num : u32 =x.rotate_left(entry.count & 0x1f);
            
            checksum = checksum.wrapping_add(num);
        }
    
        (checksum + 0x80).to_be()
    }
}


/// The RichHeader contains the entries in the Rich header, as well as the associated xor key.
/// The list of entries does not include the prelude nor trailing tag and xor key.
#[derive(Clone)]
pub struct RichHeader {
    pub checksum : u32,
    pub rich_header_entries : Vec<RichHeaderEntry>
}

impl RichHeader {
    /// to_bytes returns a Vector representation of the bytes *on disk*, where the first bytes are the 16B
    /// header prelude ["DanS", 0u32, 0u32, 0u32], followed by each entry in the rich header, and 
    /// terminated by the ascii Rich tag "Rich".
    /// 
    /// Note that as this byte repsresentation is how it should be on disk, as the entries are
    /// converted to little endian.
    pub fn to_bytes(&self) -> Vec<u8>{
        let mut ret_val : Vec<u8> = Vec::new();
        
        // prepend the Rich tag "DanS" and three 0 DWORDS
        for val in RICH_PRELUDE {
            ret_val.append(
                &mut val.to_be_bytes().to_vec()
            );
        }

        // append the Rich header entries
        for val in self.rich_header_entries.clone() {
            let mut le_bytes : Vec<u8> = Vec::new();

            le_bytes.append(
                &mut val.build.to_le_bytes().to_vec()  
            );

            le_bytes.append(
                &mut val.prog_id.to_le_bytes().to_vec()  
            );

            le_bytes.append(
                &mut val.count.to_le_bytes().to_vec()  
            );

            ret_val.append(&mut le_bytes);
        }

        // append the "Rich" string
        ret_val.append(&mut RICH_TAG.to_vec());

        ret_val
    }
    
    // TODO

    pub fn to_bytes_encoded(&mut self, key : u32) -> Vec<u8>{
        let mut ret_val : Vec<u8> = self.to_bytes();
        
        
        let key_bytes = key.to_be_bytes();
        ret_val.append(&mut key_bytes.to_vec());

        for i in 0..(ret_val.len() - 8){
            ret_val[i] ^= key_bytes[i%4];
        }

        ret_val
    }
}


pub fn randomize_header(filename : & str) -> Result<Vec<u8>,Box<dyn Error>> {
    let mut prog_ids_added : Vec<u16> = Vec::new();
    let mut builds_used: Vec<u16> = Vec::new();

    // read in the header
    let mut fullhdr = match read_headers(filename){
        Ok(x) => x,
        Err(y) => return Err(y)
    };
    // randomize the values
    for mut val in &mut fullhdr.rich_hdr.rich_header_entries {
        let mut _prog_id = (rand::random::<u16>() % 216) + 2;
        let mut _build = (rand::random::<u16>() % 30000) + (rand::random::<u16>() % 30000);

        while prog_ids_added.contains(&_prog_id) && builds_used.contains(&_build) {
            _prog_id = (rand::random::<u16>() % 200) + 1;
            _build = (rand::random::<u16>() % 5000) + 500;
        } 

        val.count = (rand::random::<u32>() % 200) + 1;
        val.prog_id = _prog_id;
        val.build = _build;

        prog_ids_added.push(_prog_id);
        builds_used.push(_build);
    }

    // calculate the checksum
    let checksum = fullhdr.get_checksum();


    // get the bytes from the newly edited header
    let fullheader_bytes = fullhdr.to_bytes_encoded(checksum);
    
    Ok(fullheader_bytes)
}

pub fn write_headers(filename : &str, header_bytes : &mut [u8]) -> Result<(),Box<dyn Error>>{
    let mut file =  match OpenOptions::new().write(true).open(filename){
        Ok(x) => x,
        Err(y) => return Err(Box::new(y))
    };
    
    file.write_all(&header_bytes).unwrap();     
    Ok(())
}

pub fn read_headers(filename : &str) -> Result<FullDosRichHeader, Box<dyn Error>>{
    let mut rich_hdr_entries : Vec<RichHeaderEntry> = Vec::new();    
    let xor_key : u32;

    let mut file =  match File::open(filename){
        Ok(x) => x,
        Err(y) => {
            
            return Err(Box::new(y))
        }
    };
    
    let mut dos_header_bytes : Vec<u8> = vec![0;std::mem::size_of::<ImageDosHeader>()];

    match file.read_exact(dos_header_bytes.as_mut_slice()){
        Ok(_) => {},
        Err(y) => return Err(Box::new(y))
    }

    let dos_hdr : &ImageDosHeader = unsafe {
        std::mem::transmute::<*mut u8, &ImageDosHeader>(dos_header_bytes.as_mut_ptr())
    };

    let sz_rich_hdr = dos_hdr.e_lfanew as usize - RICH_HEADER_OFFSET as usize;
    println!("Rich header is {:x} bytes (plus padding)", sz_rich_hdr);

    let mut rich_hdr_bytes : Vec<u8> = vec![0u8; sz_rich_hdr];

    match file.seek(SeekFrom::Start(RICH_HEADER_OFFSET)){
        Ok(_) => {},
        Err(y) => return Err(Box::new(y))
    }
    
    match file.read_exact(rich_hdr_bytes.as_mut_slice()){
        Ok(_) => {},
        Err(y) => return Err(Box::new(y))
    }

    // the first 4B of the header are DanS ^ key
    // the next 16B are 0x0000 ^ key, aka the key
    xor_key = u32::from_be_bytes(rich_hdr_bytes[4..8].try_into().unwrap());
    let entry_offset : usize = 0x10;

    println!("Parsing Rich Header Entries...");

    for i in (entry_offset..rich_hdr_bytes.len()).step_by(8) {
        // the first 4B of an entry are the build,prod_id
        // the next 4B are the count
        if rich_hdr_bytes[i..i+4] == RICH_TAG {
            break;
        }

        let mut first_u32 : u32 = u32::from_be_bytes(rich_hdr_bytes[i..i+4].try_into().unwrap());
        let mut second_u32 : u32 = u32::from_be_bytes(rich_hdr_bytes[i+4..i+8].try_into().unwrap());

        first_u32 ^= xor_key;
        second_u32 ^= xor_key;
        
        let build : u16 = u16::from_le_bytes(first_u32.to_be_bytes()[0..2].try_into().unwrap());
        let prog_id : u16 = u16::from_le_bytes(first_u32.to_be_bytes()[2..4].try_into().unwrap());
        let count : u32 = u32::from_le_bytes(second_u32.to_be_bytes()[0..4].try_into().unwrap());
        
        rich_hdr_entries.push(RichHeaderEntry{
            build: build, 
            prog_id,
            count : count   
        });
    }
    
    let mut dos_stub_vec = vec![0u8; 0x40];
    match file.seek(SeekFrom::Start(std::mem::size_of::<ImageDosHeader>() as u64)){
        Ok(_) => {},
        Err(y) => return Err(Box::new(y))
    }

    match file.read_exact(dos_stub_vec.as_mut_slice()){
        Ok(_) => {},
        Err(y) => return Err(Box::new(y))
    }


    // read DWORDs until we get "Rich" or die trying

    let ret : FullDosRichHeader = FullDosRichHeader {
        dos_hdr: *dos_hdr,
        dos_hdr_stub: ImageDosHeaderStub { content: dos_stub_vec },
        rich_hdr : RichHeader {
            checksum : xor_key,
            rich_header_entries : rich_hdr_entries
        }
    };
    Ok(ret)
}

#[cfg(test)]
mod tests {

    use crate::{randomize_header, read_headers, write_headers};

    #[test]
    fn test_checksum() {
        let filename = r#"C:\Windows\Temp\original.exe"#;
        let fullhdr = match read_headers(filename){
            Ok(x) => x,
            Err(_) => return
        };

        let checksum = fullhdr.get_checksum();
        if checksum != fullhdr.rich_hdr.checksum {
            panic!("SHIT THE CHECKSUMS DONT MATCH SHIT SHIT SHIT")
        }
        println!("Checksums match!")
    }

    #[test]
    fn test_randomize() {
        let filename = r#"C:\Windows\Temp\randomized.exe"#;

        let mut bytes = match randomize_header(filename){
            Ok(x) => x,
            Err(_) => { return }
        };

        match write_headers(filename, bytes.as_mut_slice()) {
            Ok(_) => {
                println!("Wrote scrambled headers to file");
            }
            Err(_) => {
                println!("Error writing to file")
            }
        }     
    }
}
