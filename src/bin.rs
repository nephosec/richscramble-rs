use std::env;
use richscramble::{randomize_header, write_headers};

const HELP_MSG : &'static str = r#"
Usage:
    prog.exe file
"#;


pub fn main() {
    let args : Vec<String> = env::args().collect();

    if args.len() != 2 {
        println!("{}",HELP_MSG);
        return
    }

    let mut bytes = match randomize_header(&args[1]){
        Ok(x) => x,
        Err(_) => { return }
    };

    match write_headers(&args[1], bytes.as_mut_slice()) {
        Ok(_) => {
            println!("Wrote scrambled headers to file");
        }
        Err(y) => {
            println!("Error writing to file - {}", y.as_ref())
        }
    }
}