### What is this?

Idk man, it just randomizes the rich headers of a PE

### What do I do?

Just clone it, `cargo build` and enjoy

### Why does this matter?

It probably won't for you, but in any case here's some links I used to justify spending 10h on this

* https://gist.github.com/skochinsky/07c8e95e33d9429d81a75622b5d24c8b
* https://github.com/trailofbits/pe-parse/blob/f0097d2d9f32cd329a80e89f01dc2b9918cbe149/pe-parser-library/src/parse.cpp#L1183
* https://www.virusbulletin.com/virusbulletin/2020/01/vb2019-paper-rich-headers-leveraging-mysterious-artifact-pe-format/
* https://blog.kwiatkowski.fr/?q=en/rich-header
* https://securelist.com/the-devils-in-the-rich-header/84348/

### Useage

```
cargo build --release
.\target\release\scrambler.exe the_thing_youre_trying_to_mangle.exe
```

That's it.